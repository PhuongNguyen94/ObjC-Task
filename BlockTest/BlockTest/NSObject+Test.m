//
//  NSObject+Test.m
//  BlockTest
//
//  Created by Administrator on 2/8/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

#import "NSObject+Test.h"


typedef void (^MyBlockType)(id, NSUInteger, BOOL *);

@interface Test ()

@property (nonatomic, copy) MyBlockType block;
@end

@implementation Test {
    int _gameCount;
}

- (void)runTests {
    
    // 1: declaring and calling a block
    void (^MyBlock)(id, NSUInteger, BOOL*) = ^(id obj, NSUInteger idx, BOOL *stop) {
        NSLog(@"Video game: %@", (NSString *)obj);
    };
    MyBlock(@"Path of Exile", 0, NO);
    
    // 2: passing a block to a method
    NSArray *videoGames = @[@"Fallout 2", @"Deus Ex", @"Final Fantasy IV"];
    [videoGames enumerateObjectsUsingBlock:MyBlock];
    
    // 2: inline form
    [videoGames enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSLog(@"Video game: %@", (NSString *)obj);
    }];
    
    // Stop
    
    // 3: calling a method with a block as a parameter
    [self doSomethingWithBlock:MyBlock];
    

    NSString *favoriteGame = @"Fallout 2";
    [videoGames enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *game = (NSString *)obj;
        if ([game isEqualToString:favoriteGame]) {
            NSLog(@"w00t, %@ is my favorite game!", (NSString *)obj);
        } else {
            NSLog(@"Video game: %@", (NSString *)obj);
        }
    }];
    
    // 6: __block variables
    __block int countOfGames = 0;
    [videoGames enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSLog(@"Count of games: %d", countOfGames);
        countOfGames++;
    }];
    NSLog(@"Final Count of games: %d", countOfGames);
    
}

- (void)logDone {
    NSLog(@"Done!");
}


- (void)doSomethingWithBlock:(MyBlockType)block {
    self.block = block;
    [self performSelector:@selector(afterOneSecond) withObject:self afterDelay:1.0];
}

- (void)afterOneSecond {
    _block(@"The Legend of Zelda", 0, NO);
}

- (void)dealloc {
    NSLog(@"BlockTester deallocated!");
}

@end

