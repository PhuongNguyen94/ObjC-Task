//
//  ViewController.m
//  BlockAndThread1
//
//  Created by Administrator on 2/9/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];


}
-(void) loadData:(void (^)(NSData* data,int *index))completion{
    
    NSArray *imgs = [NSArray arrayWithObjects:@"https://upload.wikimedia.org/wikipedia/en/thumb/2/21/Web_of_Spider-Man_Vol_1_129-1.png/250px-Web_of_Spider-Man_Vol_1_129-1.png", @"http://media.beliefnet.com/~/media/ilovejesus/superheroes/spiderman2_1600_1024x768.jpeg?as=1&w=400", @"https://news.marvel.com/wp-content/uploads/2017/06/02_SpiderMan_post_master-6-960x540.jpg",@"https://vignette.wikia.nocookie.net/batman/images/f/ff/Amygdala_-_New_52.jpg/revision/latest/scale-to-width-down/250?cb=20131115231406",nil];
    dispatch_queue_t myQueue = dispatch_queue_create("thread1",NULL);
   
    for(int i;i<[imgs count]-1;i++){
        dispatch_async(myQueue, ^{
            NSURL *url = [[NSURL alloc]initWithString:imgs[i]];
            NSData *data = [[NSData alloc]initWithContentsOfURL:url];
            completion(data,i);
        });
    }
}
- (IBAction)playButton:(id)sender {
    
    [self loadData:^(NSData *data, int *index) {
        dispatch_async(dispatch_get_main_queue(), ^{
            UIImageView *img = [self.view viewWithTag:(100+index)];
            img.image = [[UIImage alloc]initWithData:data];
        });
       
    }];
}

@end
