//
//  ViewController.m
//  TrainingObjC
//
//  Created by conchjmnon on 2/21/18.
//  Copyright © 2018 NguyenDuyPhuong. All rights reserved.
//

#import "LoginViewController.h"
#import "PageCell.h"
#import "NSObject+Page.h"
#import "LogginCell.h"

@interface LoginViewController ()
 @property (weak, nonatomic) IBOutlet UICollectionView *PageCollectionView;

@property (atomic)int pageNumber;
@end

@implementation LoginViewController
{
    NSMutableArray *pages;
    
  
}


- (void)viewDidLoad {
    [super viewDidLoad];
    Page *page = [[Page alloc]init];
    [page setImage:@"page1"];
    [page setTitle:@"Share a great listen"];
  
    
    
    Page *page1 = [[Page alloc]init];
    [page1 setImage:@"page2"];
    [page1 setTitle:@"Send from your library"];

    
    Page *page2 = [[Page alloc]init];
    [page2 setImage:@"page3"];
    [page2 setTitle:@"Send from the player"];

    self->pages = [[NSMutableArray alloc]initWithObjects:page,page1,page2, nil];
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return pages.count + 1 ;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"cell";
    static NSString *identifier1 = @"cellLogin";
    if (indexPath.item == pages.count){
        LogginCell *logincell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier1 forIndexPath:indexPath];
        
        
        return logincell;
    }
    
    PageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.item = pages[indexPath.row];
    cell.pageController.currentPage = self.pageNumber;
    
    return cell;
}
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    decelerate = YES;
    
}
- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView
                     withVelocity:(CGPoint)velocity
              targetContentOffset:(inout CGPoint *)targetContentOffset{
    self->_pageNumber = (int)((*targetContentOffset).x /self.view.frame.size.width);
    
    NSLog(@"%d",_pageNumber);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section{
    return 0;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.PageCollectionView.frame.size.width, self.PageCollectionView.frame.size.height);
}



- (void)finishLoggingIn {
    NSLog(@"123");
    [self dismissViewControllerAnimated:YES completion:nil];
}



@end
