//
//  HomeNavigation.m
//  TrainingObjC
//
//  Created by DuyPhuong on 2/26/18.
//  Copyright © 2018 NguyenDuyPhuong. All rights reserved.
//

#import "HomeNavigation.h"
#import "LoginViewController.h"
#import "ViewController2.h"

@interface HomeNavigation ()

@end

@implementation HomeNavigation


- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor redColor];

    if (self.isLogginIn){

       [self performSelector:@selector(showView2) withObject:nil afterDelay:0.01];
    
    }else{
        [self performSelector:@selector(showLoginController) withObject:nil afterDelay:0.01];
    }
}
-(BOOL)isLogginIn{
    return NO;
}
//MARK : - show view 2
-(void)showView2{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LoginViewController *vc = (LoginViewController*)[mainStoryboard instantiateViewControllerWithIdentifier:@"view2"];
    UINavigationController *navControl = [[UINavigationController alloc] initWithRootViewController: vc];
    [self presentViewController:navControl animated:YES completion:^{
    
    }];
}
//MARK : show view login
-(void)showLoginController{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    LoginViewController *vc = (LoginViewController*)[mainStoryboard instantiateViewControllerWithIdentifier:@"LoginVc"];
    UINavigationController *navControl = [[UINavigationController alloc] initWithRootViewController: vc];
    [self presentViewController:navControl animated:YES completion:^{
        
    }];
}


@end
