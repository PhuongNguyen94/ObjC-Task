//
//  HomeNavigation.h
//  TrainingObjC
//
//  Created by DuyPhuong on 2/26/18.
//  Copyright © 2018 NguyenDuyPhuong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeNavigation : UINavigationController
-(BOOL)isLogginIn;
@end
