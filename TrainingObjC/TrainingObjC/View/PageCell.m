//
//  PageCell.m
//  TrainingObjC
//
//  Created by conchjmnon on 2/21/18.
//  Copyright © 2018 NguyenDuyPhuong. All rights reserved.
//

#import "PageCell.h"

@implementation PageCell
@synthesize item;

-(void)setItem:(Page *)item{
    self.imageView.image = [UIImage imageNamed:item.image];
    self.textView.text = item.title;
};

@end
