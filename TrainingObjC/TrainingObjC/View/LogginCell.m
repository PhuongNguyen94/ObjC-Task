//
//  LogginCell.m
//  TrainingObjC
//
//  Created by DuyPhuong on 2/26/18.
//  Copyright © 2018 NguyenDuyPhuong. All rights reserved.
//

#import "LogginCell.h"
#import "LoginViewController.h"

@implementation LogginCell

- (IBAction)loginButton:(id)sender {
    LoginViewController *vc = [[LoginViewController alloc]init];
    [vc finishLoggingIn];
    NSLog(@"click button login");
}
@end
