//
//  LogginCell.h
//  TrainingObjC
//
//  Created by DuyPhuong on 2/26/18.
//  Copyright © 2018 NguyenDuyPhuong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogginCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UITextField *UserName;
@property (weak, nonatomic) IBOutlet UITextField *passWord;

@end
