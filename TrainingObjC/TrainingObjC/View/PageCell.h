//
//  PageCell.h
//  TrainingObjC
//
//  Created by conchjmnon on 2/21/18.
//  Copyright © 2018 NguyenDuyPhuong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NSObject+Page.h"



@interface PageCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageController;
@property (retain,nonatomic)NSNumber* pageNumber;
@property (nonatomic)Page *item;
@end
