//
//  NSObject+Page.h
//  TrainingObjC
//
//  Created by conchjmnon on 2/21/18.
//  Copyright © 2018 NguyenDuyPhuong. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Page : NSObject
@property(weak,nonatomic) NSString *title;
@property(weak,nonatomic) NSString *image;
-(instancetype)initPage:(NSString*)title : (NSString*)image;

@end

