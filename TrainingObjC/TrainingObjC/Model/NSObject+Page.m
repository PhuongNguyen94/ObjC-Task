//
//  NSObject+Page.m
//  TrainingObjC
//
//  Created by conchjmnon on 2/21/18.
//  Copyright © 2018 NguyenDuyPhuong. All rights reserved.
//

#import "NSObject+Page.h"

@implementation Page:NSObject
@synthesize image;
@synthesize title;

//MARK : - hàm khởi tạo không có tham số ->tự khởi tạo khi gọi init
-(instancetype)init{
    self = [super init];
    if (self){
        NSLog(@"init");
    }
    return  self;
}
//MARK : - hàm khởi tạo có tham số .
-(instancetype)initPage:(NSString*)title : (NSString*)image{
    self = [super init];
    self.title = title;
    self.image = image;
    return self;
}


@end
