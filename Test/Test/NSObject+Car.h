//
//  NSObject+Car.h
//  Test
//
//  Created by Administrator on 2/8/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSObject+Person.h"

@interface Car : NSObject
@property BOOL running;
@property (nonatomic)NSString *model;
//driver là property kiểu strong, nên đối tượng honda có quyền sở hữu đối tượng john -> giá trị của john vẫn tồn tại chừng nào đối tượng honda còn tham chiếu đến nó.
//->>
//Mặc định của con trỏ là strong.
//Dùng strong pointer để giữ đối tượng mà mình tạo ra.
//Không dùng strong pointer để giữ một đối tượng không thực sự phải của mình >> >> Không giải phóng được bộ nhớ ->Ta sẽ dùng weak pointer:
@property (nonatomic,strong)Person *driver;
@end
