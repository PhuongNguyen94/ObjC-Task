//
//  NSObject+Person.h
//  Test
//
//  Created by Administrator on 2/8/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

#import <Foundation/Foundation.h>

//Đảm bảo class Car tồn tại,sử dụng thay vì sử dụng từ khóa #import như thông thường vì lớp Car cũng tiến hành #import “Person.h”, nếu làm như thế ta sẽ có vòng lặp vô tận import.
@class Car;
@interface Person:NSObject
@property (nonatomic) NSString*name;

@property(nonatomic,strong) Car*bike;//-->> @property (nonatomic, weak) Car *car;

@end
