//
//  main.m
//  Test
//
//  Created by Administrator on 2/8/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "NSObject+Car.h"
#import "NSObject+Person.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        //vd1
        Person *john = [[Person alloc]init];john.name =@"John";
        Car *honda = [[Car alloc]init];honda.model = @"Honda Civic";honda.driver = john;
        NSLog(@"%@ is driving the %@",honda.driver,honda.model);
        
        
        //vd2
        Person *jack = [[Person alloc]init];jack.name = @"Jack";
        Car *bike = [[Car alloc]init];bike.model = @"Vision";bike.driver = jack;jack.bike = bike;
//   quan hệ sở hữu từ bike tới jack, và quan hệ sở hữu khác từ jack về bike -> 2 đối tượng trên luôn luôn sở hữu nhau. -> không thể hủy hai đối tượng này, ngay cả khi chúng không còn cần thiết.
//  ->>Chu trình retain (retain cycle), là một hình thức rò rỉ bộ nhớ. May mắn thay,khắc phục vấn đề này, ta chỉ cần thay thế thuộc tính strong của property bằng thuộc tính weak – một tham chiếu yếu sẽ được tạo ra từ property này đến đối tượng khác.@property (nonatomic, weak) Car *car;
//
//        Thuộc tính weak thường được sử dụng đối với cấu trúc dữ liệu kiểu cha-con. Quy ước, đối tượng cha cần duy trì một tham chiếu mạnh đến đối tượng con, trong khi đối tượng con có một tham chiếu yếu trở lại đối tượng cha. Tham chiếu yếu cũng là một phần quan trọng của delegate design pattern.
        
    }
    
    return 0;
}
