//
//  ViewController.m
//  ThreadDemo
//
//  Created by Administrator on 2/9/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *img_View;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createThreadToLoadImage];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//MARK : - NSThread


//MARK : - Create new thread

-(void) createThreadToLoadImage {
    
    NSString *imageURL = @"https://upload.wikimedia.org/wikipedia/en/thumb/2/21/Web_of_Spider-Man_Vol_1_129-1.png/250px-Web_of_Spider-Man_Vol_1_129-1.png";
    NSThread *myThread = [[NSThread alloc]initWithTarget:self selector:@selector(processLoadingImageInNewThread:) object:imageURL];
    [myThread start];
    
    // Sử dụng khi load nhiều hình ->tạo nhiều thread -> CPU hoạt động nhiều - > Sử dụng NSoperationQueue .
    
//    for( id imageURL in imageURLs)
//    {
//        NSThread* myThread = [[NSThread alloc] initWithTarget:self
//                                                     selector:@selector(processLoadingImageInNewThread:)
//                                                       object:imageURL];
//        [myThread start];
//    }
    
    //--->>> Load image su dung NSOperationQueue
    
//    - (void) usingNSOperationQueueToLoadImage:(NSArray*) imageURLs
//    {
//        NSOperationQueue *operationQueue = [NSOperationQueue new];
//        [operationQueue setMaxConcurrentOperationCount:10];
//        for( id imageURL in imageURLs)
//        {
//            NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:
//                                                                                    selector:@selector(processLoadingImageInNewThread:)
//                                                                                      object:imageURL];
//            [operationQueue addOperation:operation];
//            [operation release];
//        }
//        [operationQueue start];
//    }
    
    
    
    //  Load image su dung GCD
    
    
//    - (void) usingGCDToLoadImage:(NSArray*) imageURLs
//    {
//        //  Code dang duoc thuc thi tren main thread
//        // Tao ra mot hang doi bang GCD
//        dispatch_queue_t myQueue = dispatch_queue_create("com.myApp.myQueue", NULL);
//        // Dua danh sach cong viec vao hang doi
//        for( id imageURL in imageURLs)
//        {
//            dispatch_async(myQueue,
//                           ^{
//                               // Code dang duoc thuc thi trong thread cua hang doi do ban tao ra, khong phai la main thread
//                               NSURL *url = [NSURL urlWithString:imageURL];
//                               NSData *imageData = [ [NSData alloc] initWithContentsOfURL: url ];
//                               // Dua ket qua ve main thread de xu ly hien thi UI
//                               dispatch_async(dispatch_get_main_queue(),
//                                              ^{
//                                                  // Code dang duoc thuc thi trong main thread
//                                                  UIImage *downloadedImage = [UIImage imageWithData:imageData];
//                                                  self.imageView.image = downloadedImage;
//                                              });
//                           }
//                           }
    
}


//MARK: - Load image from URL in new thread
-(void)processLoadingImageInNewThread:(NSString *)imageURL{
    NSURL *url = [NSURL URLWithString:imageURL];
    NSData *imageData = [[NSData alloc]initWithContentsOfURL:url];
    
    [self performSelectorOnMainThread:@selector(imageDownloadDidFinish:) withObject:imageData waitUntilDone:NO];
}

//Mọi xử lý liên quan đến các thành phần giao diện bắt buộc phải được thực thi trong main thread
//MARK: - running in main thread
-(void)imageDownloadDidFinish:(NSData*)imageData{
    UIImage *downloadedImage = [UIImage imageWithData:imageData];
    self.img_View.image = downloadedImage;
}






















@end
