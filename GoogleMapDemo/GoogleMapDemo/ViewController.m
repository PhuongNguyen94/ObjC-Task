//
//  ViewController.m
//  GoogleMapDemo
//
//  Created by Administrator on 2/5/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

#import "ViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <GooglePlaces/GooglePlaces.h>
#import <GooglePlacePicker/GooglePlacePicker.h>




@interface ViewController ()<GMSMapViewDelegate,searchButtonDelage>

@property (strong,nonatomic) GMSMapView *mapView;
@property (copy,nonatomic) NSSet *markers;
@property (strong,nonatomic) NSURLSession *markerSession;
@property (strong,nonatomic)NSNumber *lat;
@property (strong,nonatomic)NSNumber *lng;


@end

@implementation ViewController
//MARK : - Delegate 
-(void)getData:(NSNumber *)value1 and:(NSNumber *)value2{
    self.lat = value1;
    self.lng = value2;
    //MARK : - Show new camera after search .
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[self.lat doubleValue] longitude:[self.lng doubleValue] zoom:15 bearing:0 viewingAngle:0];
    self.mapView.camera = camera;

}

-(BOOL)prefersStatusBarHidden{
    return YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    


}
-(void)viewWillAppear:(BOOL)animated{

    //MARK : Show Map
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[self.lat doubleValue] longitude:[self.lng doubleValue] zoom:15 bearing:0 viewingAngle:0];
    self.mapView = [GMSMapView mapWithFrame:self.view.bounds camera:camera];
    self.mapView.myLocationEnabled = YES;
    self.mapView.settings.myLocationButton = YES;
    [self.mapView setMinZoom:10 maxZoom:18];
    [self.view addSubview:self.mapView];
    self.mapView.delegate = self;
    

    
}
-(void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position{
    GMSMarker *marker = [[GMSMarker alloc]init];
    marker.position = CLLocationCoordinate2DMake([self.lat doubleValue],[self.lng doubleValue]);
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.map = self.mapView;
 
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    ViewController3 *vc = segue.destinationViewController;
    vc.delegate = self;
    
}



@end
