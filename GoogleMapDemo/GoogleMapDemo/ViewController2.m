//
//  ViewController2.m
//  GoogleMapDemo
//
//  Created by Administrator on 2/5/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

#import "ViewController2.h"


@interface ViewController2 ()<GMSAutocompleteViewControllerDelegate>
@property (strong,nonatomic)GMSAutocompleteResultsViewController *resultsViewController;
@property (strong,nonatomic)UISearchController *searchController;
@property (strong,nonatomic) GMSMapView *mapView;


@end

@implementation ViewController2


- (void)viewDidLoad {
    [super viewDidLoad];
    _resultsViewController = [[GMSAutocompleteResultsViewController alloc] init];
    _resultsViewController.delegate = self;
    _searchController = [[UISearchController alloc]
                         initWithSearchResultsController:_resultsViewController];
    _searchController.searchResultsUpdater = _resultsViewController;
    
    _searchController.searchBar.frame = CGRectMake(0, 0, 250.0f, 44.0f);
    self.navigationItem.rightBarButtonItem =
    [[UIBarButtonItem alloc] initWithCustomView:_searchController.searchBar];
    

    self.definesPresentationContext = YES;
    
 
    _searchController.hidesNavigationBarDuringPresentation = NO;
    
    _searchController.modalPresentationStyle = UIModalPresentationPopover;
    
}
- (IBAction)clickedButton:(id)sender {
    GMSAutocompleteViewController *acController = [[GMSAutocompleteViewController alloc] init];
    acController.delegate = self;
    [self presentViewController:acController animated:YES completion:nil];
}

//MARK: - Handle the user's selection.
- (void)viewController:(GMSAutocompleteViewController *)viewController
didAutocompleteWithPlace:(GMSPlace *)place {
    [self dismissViewControllerAnimated:YES completion:nil];

    //MARK : -  Show Map
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:place.coordinate.latitude longitude:place.coordinate.longitude zoom:15 bearing:0 viewingAngle:0];
    self.mapView = [GMSMapView mapWithFrame:self.view.bounds camera:camera];
    [self.mapView setMinZoom:10 maxZoom:18];
    _mapView.frame = CGRectMake(0, 70, self.view.frame.size.width, self.view.frame.size.height);
    [self.view addSubview:self.mapView];
    
    //MARK : - Create Marker for location.
    GMSMarker *marker = [[GMSMarker alloc]init];
    marker.position = CLLocationCoordinate2DMake(place.coordinate.latitude,place.coordinate.longitude );
    marker.title = place.name;
    marker.snippet = place.formattedAddress;
    marker.appearAnimation = kGMSMarkerAnimationPop;
    marker.map = self.mapView;
    
}
- (void)viewController:(GMSAutocompleteViewController *)viewController
didFailAutocompleteWithError:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
    // TODO: handle the error.
    NSLog(@"Error: %@", [error description]);
}

// User canceled the operation.
- (void)wasCancelled:(GMSAutocompleteViewController *)viewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// Turn the network activity indicator on and off again.
- (void)didRequestAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)didUpdateAutocompletePredictions:(GMSAutocompleteViewController *)viewController {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}






@end
