    //
//  ViewController3.m
//  GoogleMapDemo
//
//  Created by Administrator on 2/6/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

#import "ViewController3.h"

@interface ViewController3 ()

@property (weak, nonatomic) IBOutlet UITextField *txt_place;



@end

@implementation ViewController3 {
    NSNumber *lat;
    NSNumber *lng;

}

NSString *APIKey = @"AIzaSyCROn5Qa2GMSBrH43cM-jlyWq3LvFg0qY8&query";

- (void)viewDidLoad {
    [super viewDidLoad];

}
//MARK : - search place with API 

-(void)searchPlace: (NSString*)place{
    
    NSString *urlStr = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/textsearch/json?key=%@=%@",APIKey,place];
    NSString *urlsString = [urlStr stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    NSURL *url = [NSURL URLWithString:urlsString];
    NSURLRequest *urlrequest = [NSURLRequest requestWithURL:url];
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *urlSection = [NSURLSession sessionWithConfiguration:config];
    NSURLSessionTask *task = [urlSection dataTaskWithRequest:urlrequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if(!error){
            if(data){
                NSDictionary *mydict = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:nil];
                NSDictionary *infoPlace = mydict[@"results"];
                for ( NSDictionary *result in infoPlace){
                    NSDictionary *infolocation= result[@"geometry"];
                    NSDictionary *location = infolocation[@"location"];
                    NSArray *value = location.allValues;
                    lat = value[0];
                    lng = value[1];
                    //MARK : - running in main thread.
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.navigationController popViewControllerAnimated:true];
                    });

                }
            }
        }
    }];
    [task resume];
}
- (IBAction)searchButton:(id)sender {
    [self searchPlace:self.txt_place.text];
    
}
-(void)viewWillDisappear:(BOOL)animated{
    [self.delegate getData:lat and:lng];
}


@end
