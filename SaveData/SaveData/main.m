//
//  main.m
//  SaveData
//
//  Created by Administrator on 2/26/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
