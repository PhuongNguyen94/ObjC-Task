//
//  ViewController.m
//  BlockDemo
//
//  Created by Administrator on 2/6/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
-(void)addNumber:(int)numberA withNumber:(int)numberB andCompletionHandle:(void (^)(int result))completionHandle{
    int result = numberA + numberB ;
    if(completionHandle){
        completionHandle(result);
    }

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    
    
    
    

    _in = ^(){
        NSLog(@"info");
    };
    _in();


    myblock = ^(int num){
        NSLog(@"%d",num);
        return num;
    };
    myblock(7);


    inso = ^(int so1 ,int so2){
        int tong = so1 + so2;
        NSLog(@"%d",tong);
    };
    inso(4,5);


    //MARK: - su dung bien dia phuong khai bao truoc do

     int i=100;
    NSLog(@"i origin %d",i);
    //lấy được giá trị của biến bên ngoài hàm!!!
    void (^inI)(void)=^(){
        NSLog(@"%d",i+50);

    };
    //MARK : - block tham chieu toi gia tri, ko tham chieu vung nho .
    i = 200;
    NSLog(@"i after change %d",i);
    inI();

    [self testMethod];
    
    
    
    
    
    
    
    
}
//
//    Sử dụng __block, biến có thể bị thay đổi giá trị khi đi qua block -> bắt giá trị ( sự kiện ) cho lần thay đổi gần nhất .
//    Ko sử dụng __block, biến không thể bị thay đổi giá trị khi đi qua block
//
//
//MARK : - Nếu muốn thay đổi giá trị của biến đã bị bắt bên trong block, sử dụng __block :

- (void) testMethod{
    __block int anInteger = 42;

    void (^testBlock)(void) = ^{
        NSLog(@"the Integer is: %i", anInteger+20);
    };

    anInteger = 84;
    NSLog(@"%i",anInteger);
    testBlock();

}





//MARK : - block khong co tham so truyen vao
void (^_in)(void);



//MARK : - ten block my block, kieu tra ve int, tham so truyen vao int
int (^myblock)(int);


//MARK : - block co nhieu tham so truyen vao ,ten block inso,2 tham so truyen vao kieu int, tra ve kieu void
void (^inso)(int,int);

@end


