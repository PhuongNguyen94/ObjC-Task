//
//  ViewController.h
//  BlockDemo
//
//  Created by Administrator on 2/6/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController
-(void)addNumber:(int)numberA withNumber:(int)numberB andCompletionHandle: (void(^)(int result))completionHandle;
@end

