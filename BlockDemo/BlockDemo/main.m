//
//  main.m
//  BlockDemo
//
//  Created by Administrator on 2/6/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "ViewController.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        ViewController *viewcontroller = [[ViewController alloc]init];
        [viewcontroller addNumber:10 withNumber:7 andCompletionHandle:^(int result) {
            NSLog(@"%i",result);
        }];
       
    }
     return 0;
}
